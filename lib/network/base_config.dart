import 'package:dio/dio.dart';

import '../enums/app_enum.dart';

mixin BaseConfig {

  var mBaseUrl = "https://dev.elred.io";

  APIState? currentApiState;
  dynamic apiResponse;
  String? currentEndPoint;

  Dio _getApiConfig() {
    var options = BaseOptions(
        followRedirects: false,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        });
    Dio dio = Dio(options);
    dio.interceptors.add(LogInterceptor(responseBody: true));
    return dio;
  }

  Dio _getApiConfigHeader() {
    var options = BaseOptions(
        baseUrl: mBaseUrl,
        followRedirects: false,
        contentType: Headers.multipartFormDataContentType,
        headers: {
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiclhnY1Y2YXh3eVRobTNQdE04aGtSaXJTQ2ZsMiIsImlhdCI6MTY5NTIzMTc5MSwiZXhwIjoxNjk2NTI3NzkxfQ.9W-QURjNQWAtZIqzYB3-yvJWeCayXvojHcIRmjpVD4A",
        });
    Dio dio = Dio(options);
    dio.interceptors.add(LogInterceptor(responseBody: true));
    return dio;
  }

  Dio _getApiConfigHeader2() {
    var options = BaseOptions(
        baseUrl: mBaseUrl,
        followRedirects: false,
        contentType: Headers.jsonContentType,
        headers: {
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiclhnY1Y2YXh3eVRobTNQdE04aGtSaXJTQ2ZsMiIsImlhdCI6MTY5NTIzMTc5MSwiZXhwIjoxNjk2NTI3NzkxfQ.9W-QURjNQWAtZIqzYB3-yvJWeCayXvojHcIRmjpVD4A",
        });
    Dio dio = Dio(options);
    dio.interceptors.add(LogInterceptor(responseBody: true));
    return dio;
  }

  Future<Response> getApiCalling(Map<String, dynamic> req,
      String apiEndPoints) async {
        var response =
        await _getApiConfig().get(apiEndPoints, queryParameters: req);
        return response;
  }

  Future<Response> downloadApiCalling(String path,
      String apiEndPoints) async {

    var response =
    await _getApiConfig().download(apiEndPoints, path);
    return response;
  }

  Future<Response> postApiCalling(Map<String, dynamic> req,
      String apiEndPoints) async {
    var response =
    await _getApiConfigHeader2().post(apiEndPoints, data: req);
    return response;
  }

  Future<Response> postImageApiCalling(FormData req,
      String apiEndPoints) async {
    var response = await _getApiConfigHeader().post(apiEndPoints, data: req);
    return response;
  }


}