import 'package:flutter/material.dart';

mixin AppDialog {
  bool isPopupOpen = false;

  showCustomPopup(
      {required Widget widget, required BuildContext context}) {
    if (isPopupOpen) {
      dismissPopup(context: context);
    }
    isPopupOpen = true;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            content: widget,
          );
        });
  }

  dismissPopup({required BuildContext context}) {
    if (isPopupOpen) {
      isPopupOpen = false;
      Navigator.pop(context);
    }
  }

  showCustomBottomPopup(
      {required Widget widget, required BuildContext context, Color? bgColor}) {
    if (isPopupOpen) {
      dismissPopup(context: context);
    }
    isPopupOpen = true;
    showModalBottomSheet(
        isDismissible: false,
        isScrollControlled: true,
        enableDrag: false,
        context: context,
        backgroundColor: bgColor,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        builder: (context) {
          //return widget;
          return AnimatedPadding(
            padding: MediaQuery.of(context).viewInsets,
            duration: const Duration(milliseconds: 100),
            curve: Curves.decelerate,
            child: widget,
          );
          /* return StatefulBuilder(builder: (context, StateSetter setStates) {

          });*/
        });
  }

  showLoader(context) {
    showCustomBottomPopup(widget: WillPopScope(
      onWillPop: () async {
        dismissPopup(context: context);
        return true;
      },
      child: Container(
        alignment: Alignment.center,
        color: Colors.grey.withOpacity(0.4),
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(color: Colors.red),
          ],
        ),
      ),
    ), context: context);
  }

  showLoader2(context) {
    showCustomBottomPopup(
        widget: WillPopScope(
          onWillPop: () async {
            if (isPopupOpen) {
              dismissPopup(context: context);
            }
            return true;
          },
          child: Container(
            alignment: Alignment.center,
            color: Colors.transparent,
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(color: Colors.red),
              ],
            ),
          ),
        ),
        context: context,
        bgColor: Colors.transparent);
  }


}