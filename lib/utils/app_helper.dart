
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';

mixin AppHelper {

  void pickedImage(context, File? selectedImage);

  Future<CroppedFile?> cropImage(context, File selectedFile) async {
    final croppedFile = await ImageCropper().cropImage(sourcePath:selectedFile.path,
      compressFormat: ImageCompressFormat.png,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false,
        ),
        IOSUiSettings(
          title: 'Cropper',
        ),
        WebUiSettings(
          context: context,
          presentStyle: CropperPresentStyle.dialog,
          boundary: const CroppieBoundary(
            width: 520,
            height: 520,
          ),
          viewPort:
          const CroppieViewPort(width: 480, height: 480, type: 'circle'),
          enableExif: true,
          enableZoom: true,
          showZoomer: true,
        ),
      ],);
    return croppedFile;
  }

  pageBuilder(Widget pageName) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => pageName,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        const curve = Curves.ease;

        var tween =
        Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  Future<dynamic> pageNavigate(BuildContext context, Widget pageName) async {
    return await Navigator.push(context, pageBuilder(pageName));
  }

  void pageNavigateReplace(BuildContext context, Widget pageName) {
    Navigator.pushReplacement(context, pageBuilder(pageName));

  }

  void showSnackBar(BuildContext context, String text, int messageType) {
    final snackBar = SnackBar(
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
           Icon(
            getIconData(messageType),
            color: Colors.white,
            size: 20,
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
               text,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      backgroundColor: getSnackBGColor(messageType),
      behavior: SnackBarBehavior.floating,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Color getSnackBGColor(messageType) {
    switch (messageType) {
      case 0:
        return Colors.green; // success
      case 1:
        return Colors.orange; // warning
      case 2:
        return Colors.red;
      default:
        return Colors.green; // success
    }
  }


  IconData getIconData(messageType) {
    switch (messageType) {
      case 0:
        return Icons.cloud_done; // success
      case 1:
        return Icons.warning; // warning
      case 2:
        return Icons.cancel;
      default:
        return Icons.cloud_done; // success
    }
  }



}