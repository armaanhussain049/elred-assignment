import 'dart:io';

import 'package:flutter/material.dart';

import '../../network/base_config.dart';
import '../../utils/app_dialog.dart';
import '../../utils/app_helper.dart';

abstract class BaseViewModel extends ChangeNotifier
    with BaseConfig, AppHelper, AppDialog {
  late BuildContext currentPageContext;
  Directory? dir;

  void onFailure(BuildContext context, String message);

  void onSuccess(BuildContext context, dynamic response);

  void loading(BuildContext context);
}
