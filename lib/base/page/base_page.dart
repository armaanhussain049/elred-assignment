
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../view_model/base_view_model.dart';

abstract class BasePage extends StatefulWidget {
  const BasePage({super.key});

  @override
  ScreenState createState();
}

abstract class ScreenState<T extends BasePage, VM extends BaseViewModel>
    extends State with WidgetsBindingObserver {
  VM? viewModel;
  ScreenState();

  Widget buildScreen(BuildContext context);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      log("initState::addPostFrameCallback");
      onResume(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    viewModel ??= context.read<VM>();
    return buildScreen(context);
  }

  void onResume(BuildContext context);

 /* @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (AppLifecycleState.resumed == state) {
      // if(mounted) {
      //   onResume(context);
      // }

    }
  }*/

}
