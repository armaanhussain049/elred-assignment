import 'package:flutter/material.dart';

import '../../../base/page/base_page.dart';
import '../../../enums/app_enum.dart';
import '../view_model/preview_image_view_model.dart';

class PreviewImagePage extends BasePage {
  const PreviewImagePage({super.key});

  @override
  PreviewImagePageState createState() => PreviewImagePageState();
}

class PreviewImagePageState
    extends ScreenState<PreviewImagePage, PreviewImageViewModel> {
  @override
  Widget buildScreen(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return viewModel?.currentApiState != APIState.loading;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Upload Picture"),
        ),
        body: Column(
          children: [
            Expanded(
                flex: 1,
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: InteractiveViewer(
                      minScale: 1.0,
                      maxScale: 2.6,
                      transformationController: viewModel?.transformController,
                      onInteractionUpdate: (value) {},
                      panEnabled:
                          viewModel?.currentApiState != APIState.loading,
                      child: Image.file(viewModel!.selectedImage,
                          fit: BoxFit.cover),
                    ),
                  ),
                )),
            const SizedBox(
              height: 10,
            ),
            const Text(
              "Picture ready to be same",
              style: TextStyle(fontSize: 17, fontFamily: "Avenir"),
            ),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: viewModel?.currentApiState != APIState.loading
                  ? () {
                      viewModel?.uploadImageToServer();
                    }
                  : null,
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                padding: const EdgeInsets.all(15),
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: viewModel?.currentApiState != APIState.loading
                      ? const  Color.fromRGBO(231, 45, 56, 1)
                      : Colors.grey,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: const Text(
                  "Save & Continue",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 16, fontFamily: "Avenir"),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void onResume(BuildContext context) {
    viewModel?.currentPageContext = context;
  }
}
