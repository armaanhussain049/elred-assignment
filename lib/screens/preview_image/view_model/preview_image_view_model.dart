import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import '../../../base/view_model/base_view_model.dart';
import '../../../enums/app_enum.dart';
import '../../../network/api_endpoints.dart';
import '../../artist/pages/artist_page.dart';
import '../../artist/view_model/artist_view_model.dart';

class PreviewImageViewModel extends BaseViewModel {
  File selectedImage;

  var transformController = TransformationController();
  PreviewImageViewModel(this.selectedImage);

  @override
  void onFailure(BuildContext context, String message) {
    dismissPopup(context: context);
    debugPrint("onFailure:: ${message.toString()}");
    showSnackBar(context, message, 1);
  }

  @override
  void onSuccess(BuildContext context, response) {
    dismissPopup(context: context);
    debugPrint("response:: ${response.toString()}");
    showSnackBar(context, response['message'], 0);
    pageNavigateReplace(
        context,
        ChangeNotifierProvider<ArtistViewModel>(
          create: (_) => ArtistViewModel(),
          child: const ArtistPage(),
        ));
  }

  uploadImageToServer() async {
    currentApiState = APIState.loading;
    notifyListeners();
    uploadProfileDetail(selectedImage);
  }

  @override
  void loading(BuildContext context) {
    showLoader2(context);
  }

  @override
  void pickedImage(context, File? selectedImage) {}

  uploadProfileDetail(File selectedImage) async {
    loading(currentPageContext);
    String fileName = selectedImage.path.split('/').last;
    FormData formData = FormData.fromMap({
      "profileBannerImageURL": await MultipartFile.fromFile(
          selectedImage.absolute.path,
          filename: fileName,
          contentType: MediaType("image", "png")),
    });

    try {
      var resp = await postImageApiCalling(
          formData, ApiEndPoints.postProfileBannerImage);
      handleResponseCode(resp);
    } on DioException catch (ex) {
      handleResponseCode(ex.response!);
    }
  }

  Future<String?> getImageDownload(profileUrl) async {
    dir = await getTemporaryDirectory();
    var resp = await downloadApiCalling('${dir?.path}/image.png', profileUrl);
    if (resp.statusCode == 200) {
      currentApiState = APIState.success;
      return '${dir?.path}/image.png';
    } else {
      currentApiState = APIState.failure;
    }
    return null;
  }

  void handleResponseCode(Response apiResp) {
    switch (apiResp.statusCode) {
      case 200:
        currentApiState = APIState.success;
        apiResponse = apiResp.data;
        onSuccess(currentPageContext, apiResponse);
        break;
      default:
        currentApiState = APIState.failure;
        apiResponse = apiResp.data['message'];
        onFailure(currentPageContext, apiResponse);
        break;
    }
  }
}
