import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '../../../base/view_model/base_view_model.dart';
import '../../preview_image/pages/preview_image_page.dart';
import '../../preview_image/view_model/preview_image_view_model.dart';

class UploadImageViewModel extends BaseViewModel {
  var imagePicker = ImagePicker();
  var test = "test";

  UploadImageViewModel();

  @override
  void onFailure(BuildContext context, String message) {
    if (isPopupOpen) {
      dismissPopup(context: context);
    }
  }

  @override
  void onSuccess(BuildContext context, response) {
    if (isPopupOpen) {
      dismissPopup(context: context);
    }
  }

  @override
  void pickedImage(context, File? selectedImage) async {
    if (selectedImage != null) {
      var file = await cropImage(context, selectedImage);
      if (file != null) {
        debugPrint("Crop Path = ${file.path}");
        uploadFileToServer(context, File(file.path));
      }
    }
  }

  void openMedia(ImageSource imageSource, context) async {
    var file = await imagePicker.pickImage(source: imageSource);
    if (file != null) {
      pickedImage(context, File(file.path));
    }
  }

  uploadFileToServer(context, File selectImage) async {
    pageNavigate(
        context,
        ChangeNotifierProvider<PreviewImageViewModel>(
          create: (_) => PreviewImageViewModel(selectImage),
          child: const PreviewImagePage(),
        ));
  }

  chooseMediaOption(BuildContext context, Widget widget) {
    showCustomBottomPopup(widget: widget, context: context);
  }

  @override
  void loading(BuildContext context) {
    showLoader(context);
  }
}
