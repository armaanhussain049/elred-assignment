import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../base/page/base_page.dart';
import '../view_model/upload_image_view_model.dart';
import '../widgets/upload_controls_view.dart';

class UploadImagePage extends BasePage {
  const UploadImagePage({super.key});

  @override
  UploadImagePageState createState() => UploadImagePageState();
}

class UploadImagePageState
    extends ScreenState<UploadImagePage, UploadImageViewModel> {
  @override
  Widget buildScreen(BuildContext context) {
    debugPrint("test:::${viewModel?.test}");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Change Design"),
      ),
      body: Column(
        children: [
          GestureDetector(
            onTap: () {
              viewModel?.chooseMediaOption(
                context,
                WillPopScope(
                  onWillPop: () async {
                    viewModel?.dismissPopup(context: context);
                    return true;
                  },
                  child: UploadImageControlsView(
                    onCameraPressed: () {
                      viewModel?.dismissPopup(context: context);
                      viewModel?.openMedia(ImageSource.camera, context);
                    },
                    onGalleryPressed: () {
                      viewModel?.dismissPopup(context: context);
                      viewModel?.openMedia(ImageSource.gallery, context);
                    },
                  ),
                ),
              );
            },
            child: Container(
              margin: const EdgeInsets.all(10),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              height: 70,
              width: double.infinity,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(216, 233, 255, 1),
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Stack(
                children: [
                  Positioned(
                    top: 5,
                      left: 10,
                      child: Image.asset("assets/images/Vector.png")),
                  const Positioned(
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    child: Center(
                      child: Text(
                        "Upload Picture",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            fontFamily: "Roboto"),
                      ),
                    ),
                  ),
                  Positioned(
                      right: 10,
                      bottom: 5,
                      child: Image.asset("assets/images/Vector.png", width: 20, height: 20,)),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void onResume(BuildContext context) {
    viewModel?.currentPageContext = context;
  }
}
