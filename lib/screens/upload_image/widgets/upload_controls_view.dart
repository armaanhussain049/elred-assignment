import 'package:flutter/material.dart';

class UploadImageControlsView extends StatelessWidget {
  final Function onCameraPressed;
  final Function onGalleryPressed;
  const UploadImageControlsView(
      {super.key,
      required this.onCameraPressed,
      required this.onGalleryPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            onTap: () {
              onCameraPressed();
            },
            child: const Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircleAvatar(
                  backgroundColor: Color.fromARGB(255, 182, 180, 180),
                  radius: 30,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 28,
                    child: Icon(Icons.camera_alt, color:  Color.fromRGBO(231, 45, 56, 1),),
                  ),
                ),
                SizedBox(height: 5,),
                Text("Camera", style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontFamily: "Avenir"
                ),)
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              onGalleryPressed();
            },
            child: const Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircleAvatar(
                  backgroundColor: Color.fromARGB(255, 182, 180, 180),
                  radius: 30,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 28,
                    child: Icon(Icons.image, color: Color.fromRGBO(231, 45, 56, 1),),
                  ),
                ),
                SizedBox(height: 5,),
                Text("Gallery", style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontFamily: "Avenir"
                ),)
              ],
            ),
          )
        ],
      ),
    );
  }
}
