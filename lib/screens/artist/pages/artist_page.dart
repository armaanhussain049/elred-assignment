import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../base/page/base_page.dart';
import '../../../enums/app_enum.dart';
import '../view_model/artist_view_model.dart';

class ArtistPage extends BasePage {
  const ArtistPage({super.key});

  @override
  ArtistPageState createState() => ArtistPageState();
}

class ArtistPageState extends ScreenState<ArtistPage, ArtistViewModel> {
  @override
  Widget buildScreen(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Artist",
        ),
      ),
      body: Column(
        children: [
          Expanded(
              flex: 1,
              child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Stack(
                      children: [
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Consumer<ArtistViewModel>(
                            builder: (BuildContext context, ArtistViewModel value, Widget? child) {
                              if(value.currentApiState == APIState.success) {
                                return Image.file(
                                  File(value.profilePath ?? ""),
                                  fit: BoxFit.cover,
                                );
                              }
                              if(value.currentApiState == APIState.loading) {
                                return const Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.red,
                                  ),
                                );
                              }
                              return const SizedBox.shrink();
                            },
                          ),

                        ),
                        const Positioned(
                          left: 0,
                          right: 0,
                          top: 40,
                          child: Column(
                            children: [
                              CircleAvatar(
                                radius: 60,
                                backgroundColor: Colors.white,
                                child: CircleAvatar(
                                  radius: 58,
                                  backgroundColor: Colors.red,
                                  child: Icon(
                                    Icons.person,
                                    size: 57,
                                  ),
                              ),
                        ),
                              SizedBox(height: 10,),
                              Text("Armaan", style: TextStyle(
                                color: Colors.white,
                                fontSize: 36,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1.3
                              ),),
                              Text("Hussain", style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  letterSpacing: 1.3
                              ),),
                              SizedBox(height: 20,),
                              Text("Realtor | VIP Design", style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  letterSpacing: 1.3
                              ),),
                              Text("Banglore, India", style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.normal,
                                  letterSpacing: 1.3
                              ),)
                            ],
                          )),
                        Positioned(
                          bottom: 30,
                          left: 0,
                          right: 0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white10.withOpacity(0.25)
                                ),
                                child: const Icon(Icons.safety_check_rounded, size: 20, color:  Colors.white,),
                              ),
                              const SizedBox(width: 10,),
                              Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white10.withOpacity(0.25)
                                ),
                                child: const Icon(Icons.messenger, size: 20, color:  Colors.white,),
                              ),
                              const SizedBox(width: 10,),
                              Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white10.withOpacity(0.25)
                                ),
                                child: const Icon(Icons.call, size: 20, color:  Colors.white,),
                              ),
                              const SizedBox(width: 10,),
                              Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white10.withOpacity(0.25)
                                ),
                                child: const Icon(Icons.location_on_sharp, size: 20, color:  Colors.white,),
                              ),
                              const SizedBox(width: 10,),
                              Container(
                                padding: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white10.withOpacity(0.25)
                                ),
                                child: const Icon(Icons.map, size: 20, color:  Colors.white,),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ))),
          const SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: viewModel?.currentApiState != APIState.loading
                ? () {
                    viewModel?.goToEdit();
                  }
                : null,
            child: Container(
              width: double.infinity,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                    color: const  Color.fromRGBO(231, 45, 56, 1), width: 1),
                color: viewModel?.currentApiState != APIState.loading
                    ? const Color.fromARGB(255, 255, 255, 255)
                    : Colors.grey,
                borderRadius: BorderRadius.circular(30),
              ),
              child: const Text(
                "Edit Card",
                style: TextStyle(
                    color:  Color.fromRGBO(231, 45, 56, 1),
                    fontWeight: FontWeight.w400,
                    fontSize: 20, fontFamily: "Avenir"),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void onResume(BuildContext context) {
    log("onResume::: artist");
    viewModel?.currentPageContext = context;
    viewModel?.loadProfileDetailFromServer();
  }
}
