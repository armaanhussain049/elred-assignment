import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import '../../../base/view_model/base_view_model.dart';
import '../../../enums/app_enum.dart';
import '../../../network/api_endpoints.dart';
import '../../../utils/app_dialog.dart';
import '../../../utils/app_helper.dart';
import '../../artist_edit_mode/pages/artist_edit_page.dart';
import '../../artist_edit_mode/view_model/artist_edit_view_model.dart';
import '../model/profile_detail_model.dart';

class ArtistViewModel extends BaseViewModel with AppDialog, AppHelper {
  String? profileUrl;
  String? profilePath;

  String? changeImagePath;
  GlobalKey? cropperKey = GlobalKey();
  Uint8List? image;

  var transformController = TransformationController();

  @override
  void loading(BuildContext context) {
     showLoader2(context);
  }

  @override
  void onFailure(BuildContext context, String message) {
    dismissPopup(context: context);
    showSnackBar(context, message, 1);
  }

  @override
  void onSuccess(BuildContext context, response) {
    dismissPopup(context: context);
    var resp = ProfileDetailModel.fromJson(response);
    profileUrl = resp.result[0].profileDesignInfo.profileBannerImageUrl;
    notifyListeners();
    getProfileImage();
  }

  getProfileImage() async {
    profilePath = await getImageDownload(profileUrl);
    notifyListeners();
  }

  loadProfileDetailFromServer() async {
    currentApiState = APIState.loading;
    notifyListeners();
    getProfileDetail();
  }

  goToEdit() {
    pageNavigate(
        currentPageContext,
        ChangeNotifierProvider<ArtistEditViewModel>(
          create: (_) => ArtistEditViewModel(profileUrl!),
          child: const ArtistEditPage(),
        )).then((value) {
      loadProfileDetailFromServer();
    });
  }

  @override
  void pickedImage(context, File? selectedImage) {}

  getProfileDetail() async {
    loading(currentPageContext);
    try {
      var resp = await postApiCalling(
          {"cardImageId": "6300ba8b5c4ce60057ef9b0c"},
          ApiEndPoints.selectedCardDesignDetails);
      handleResponseCode(resp);
    } on DioException catch (ex) {
      handleResponseCode(ex.response!);
    }
  }

  Future<String?> getImageDownload(profileUrl) async {
    var fileName = "image_${DateTime.now().millisecondsSinceEpoch}.png";
    dir = await getTemporaryDirectory();
    var resp = await downloadApiCalling('${dir?.path}/$fileName', profileUrl);
    if(resp.statusCode == 200) {
      currentApiState = APIState.success;
      return '${dir?.path}/$fileName';
    } else {
      currentApiState = APIState.failure;
    }
    return null;
  }

  Future<Uint8List?> crop({
    required GlobalKey cropperKey,
    double pixelRatio = 3,
  }) async {
    // Get cropped image
    final renderObject = cropperKey.currentContext!.findRenderObject();
    final boundary = renderObject as RenderRepaintBoundary;
    final image = await boundary.toImage(pixelRatio: pixelRatio);

    // Convert image to bytes in PNG format and return
    final byteData = await image.toByteData(
      format: ImageByteFormat.png,
    );
    final pngBytes = byteData?.buffer.asUint8List();
    return pngBytes;
  }

  updateData(context) async {
    image = await crop(cropperKey: cropperKey!);
    final tempDir = await getTemporaryDirectory();
    File file = await File('${tempDir.path}/image.png').create();
    file.writeAsBytesSync(image!);
    changeImagePath = '${tempDir.path}/image.png';
    uploadImageToServer(context);
  }

  uploadImageToServer(BuildContext context) async {
    loading(currentPageContext);
    if(changeImagePath == null) {
      showSnackBar(context, "You haven't change anything yet", 1);
    } else {
      currentApiState = APIState.loading;
      notifyListeners();
      uploadProfileDetail(File(changeImagePath!));
      notifyListeners();
    }
  }

  uploadProfileDetail(File selectedImage) async {
    loading(currentPageContext);
    String fileName = selectedImage.path.split('/').last;
    FormData formData = FormData.fromMap({
      "profileBannerImageURL": await MultipartFile.fromFile(
          selectedImage.absolute.path,
          filename: fileName,
          contentType: MediaType("image", "png")),
    });
    try {
      var resp = await postImageApiCalling(
          formData, ApiEndPoints.postProfileBannerImage);
      handleResponseCode(resp);
    } on DioException catch (ex) {
      handleResponseCode(ex.response!);
    }
  }

  void handleResponseCode(Response apiResp) {
    switch (apiResp.statusCode) {
      case 200:
        currentApiState = APIState.success;
        apiResponse = apiResp.data;
        onSuccess(currentPageContext, apiResponse);
        break;
      default:
        currentApiState = APIState.failure;
        apiResponse = apiResp.data['message'];
        onFailure(currentPageContext, apiResponse);
        break;
    }
  }
}
