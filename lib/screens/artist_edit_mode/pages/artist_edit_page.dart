import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../../base/page/base_page.dart';
import '../../../enums/app_enum.dart';
import '../../upload_image/widgets/upload_controls_view.dart';
import '../view_model/artist_edit_view_model.dart';

class ArtistEditPage extends BasePage {
  const ArtistEditPage({super.key});

  @override
  ArtistEditPageState createState() => ArtistEditPageState();
}

class ArtistEditPageState
    extends ScreenState<ArtistEditPage, ArtistEditViewModel> {
  @override
  Widget buildScreen(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Custom Image Card",
        ),
        actions: [
          Selector<ArtistEditViewModel, bool>(
              builder: (context, path, child) {
                if (path) {
                  return IconButton(
                      onPressed: () {
                        viewModel?.getProfileImage();
                      },
                      icon: const Icon(
                        Icons.clear,
                        color: Colors.black54,
                      ));
                }
                return const SizedBox.shrink();
              },
              selector: (context, path) => path.isImageChange),
        ],
      ),
      body: Column(
        children: [
          Selector<ArtistEditViewModel, bool>(
              builder: (context, path, child) {
                if (path) {
                  return GestureDetector(
                    onTap: () {
                      viewModel?.chooseMediaOption(
                          context,
                          WillPopScope(
                            onWillPop: () async {
                              viewModel?.dismissPopup(context: context);
                              return true;
                            },
                            child: UploadImageControlsView(
                              onCameraPressed: () {
                                viewModel?.dismissPopup(context: context);
                                viewModel?.openMedia(
                                    ImageSource.camera, context);
                              },
                              onGalleryPressed: () {
                                viewModel?.dismissPopup(context: context);
                                viewModel?.openMedia(
                                    ImageSource.gallery, context);
                              },
                            ),
                          ));
                    },
                    child: Container(
                      padding: const EdgeInsets.all(20),
                      margin: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Colors.blue.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blue, width: 1),
                      ),
                      child:  Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/images/Vector-2.png", width: 40, height: 30,),
                          const SizedBox(
                            width: 10,
                          ),
                          const Text(
                            "Change picture here and adjust",
                            style:
                                TextStyle(color: Colors.black54, fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                  );
                }
                return const SizedBox.shrink();
              },
              selector: (context, path) => path.isImageChange),
          Expanded(
              flex: 1,
              child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Stack(
                      children: [
                        Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: Consumer<ArtistEditViewModel>(
                            builder: (BuildContext context,
                                ArtistEditViewModel value, Widget? child) {
                              if (value.currentApiState == APIState.loading) {
                                return const Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.red,
                                  ),
                                );
                              } else if (viewModel?.profilePath != null) {
                                return RepaintBoundary(
                                  key: viewModel?.cropperKey,
                                  child: InteractiveViewer(
                                    minScale: 1.0,
                                    maxScale: 2.6,
                                    onInteractionUpdate: (value) {},
                                    panEnabled: viewModel?.currentApiState !=
                                        APIState.loading,
                                    child: Image.file(
                                      File(value.profilePath!),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                );
                              }
                              return const SizedBox.shrink();
                            },
                          ),
                        ),
                        Selector<ArtistEditViewModel, bool>(
                            builder: (context, path, child) {
                              if (!path) {
                                return Positioned(
                                  right: 10,
                                  top: 10,
                                  child: GestureDetector(
                                    onTap: () {
                                      viewModel?.chooseMediaOption(
                                          context,
                                          WillPopScope(
                                            onWillPop: () async {
                                              viewModel?.dismissPopup(
                                                  context: context);
                                              return true;
                                            },
                                            child: UploadImageControlsView(
                                              onCameraPressed: () {
                                                viewModel?.dismissPopup(
                                                    context: context);
                                                viewModel?.openMedia(
                                                    ImageSource.camera,
                                                    context);
                                              },
                                              onGalleryPressed: () {
                                                viewModel?.dismissPopup(
                                                    context: context);
                                                viewModel?.openMedia(
                                                    ImageSource.gallery,
                                                    context);
                                              },
                                            ),
                                          ));
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: const Row(
                                        children: [
                                          Icon(
                                            Icons.edit,
                                            color: Colors.red,
                                            size: 15,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "Customise",
                                            style: TextStyle(color: Colors.red),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }
                              return const SizedBox.shrink();
                            },
                            selector: (context, path) => path.isImageChange),
                        Positioned(
                            left: 0,
                            right: 0,
                            top: 40,
                            child: Column(
                              children: [
                                CircleAvatar(
                                  radius: 60,
                                  backgroundColor: Colors.white,
                                  child: CircleAvatar(
                                    radius: 58,
                                    backgroundColor: Colors.grey.shade400,
                                    child: const Icon(
                                      Icons.person,
                                      size: 57,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  "Armaan",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 36,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 1.3),
                                ),
                                const Text(
                                  "Hussain",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.normal,
                                      letterSpacing: 1.3),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const Text(
                                  "Realtor | VIP Design",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.normal,
                                      letterSpacing: 1.3),
                                ),
                                const Text(
                                  "Bangalore, India",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.normal,
                                      letterSpacing: 1.3),
                                )
                              ],
                            )),
                      ],
                    ),
                  ))),
          const SizedBox(
            height: 10,
          ),
          GestureDetector(
            onTap: viewModel?.currentApiState != APIState.loading
                ? () {
                    viewModel?.updateData(context);
                  }
                : null,
            child: Container(
              width: double.infinity,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border.all(
                    color: const Color.fromRGBO(231, 45, 56, 1), width: 1),
                color: viewModel?.currentApiState != APIState.loading
                    ? const Color.fromRGBO(231, 45, 56, 1)
                    : Colors.grey,
                borderRadius: BorderRadius.circular(30),
              ),
              child: const Text(
                "Save",
                style: TextStyle(
                    color: Color.fromARGB(255, 255, 255, 255),
                    fontWeight: FontWeight.w400,
                    fontSize: 20, fontFamily: "Avenir"),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  void onResume(BuildContext context) {
    viewModel?.currentPageContext = context;
    viewModel?.getProfileImage();
  }
}
