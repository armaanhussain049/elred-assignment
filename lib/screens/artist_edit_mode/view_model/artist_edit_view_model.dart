import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import '../../../base/view_model/base_view_model.dart';
import '../../../enums/app_enum.dart';
import '../../../network/api_endpoints.dart';

class ArtistEditViewModel extends BaseViewModel {
  var imagePicker = ImagePicker();
  File? selectedImage;
  String? profilePath;
  String profileUrl;
  bool isImageChange = false;
  GlobalKey? cropperKey = GlobalKey();
  Uint8List? image;

  ArtistEditViewModel(this.profileUrl);

  @override
  void loading(BuildContext context) {
    showLoader2(context);
  }

  @override
  void onFailure(BuildContext context, String message) {
    showSnackBar(context, message, 1);
  }

  @override
  void onSuccess(BuildContext context, response) {
    dismissPopup(context: context);
    debugPrint("response:: ${response.toString()}");
    showSnackBar(context, response['message'], 0);
    Navigator.of(context).pop(true);
  }

  @override
  void pickedImage(context, File? selectedImage) async {
    if (selectedImage != null) {
      isImageChange = true;
      var file = await cropImage(context, selectedImage);
      if (file != null) {
        debugPrint("Crop Path = ${file.path}");
        profilePath = file.path;
      }
    }
    notifyListeners();
  }

  void openMedia(ImageSource imageSource, context) async {
    var file = await imagePicker.pickImage(source: imageSource);
    if (file != null) {
      pickedImage(context, File(file.path));
    }
  }

  getProfileImage() async {
    currentApiState = APIState.loading;
    notifyListeners();
    profilePath = await getImageDownload(profileUrl);
    notifyListeners();
  }

  chooseMediaOption(BuildContext context, Widget widget) {
    showCustomBottomPopup(widget: widget, context: context);
  }

  uploadImageToServer(BuildContext context) async {
    currentApiState = APIState.loading;
    notifyListeners();
    loading(currentPageContext);
    uploadProfileDetail(File(profilePath!));
  }

  Future<Uint8List?> crop({
    required GlobalKey cropperKey,
    double pixelRatio = 3,
  }) async {
    // Get cropped image
    final renderObject = cropperKey.currentContext!.findRenderObject();
    final boundary = renderObject as RenderRepaintBoundary;
    final image = await boundary.toImage(pixelRatio: pixelRatio);

    // Convert image to bytes in PNG format and return
    final byteData = await image.toByteData(
      format: ImageByteFormat.png,
    );
    final pngBytes = byteData?.buffer.asUint8List();
    return pngBytes;
  }

  updateData(context) async {
    var fileName = "image_${DateTime.now().millisecondsSinceEpoch}.png";
    image = await crop(cropperKey: cropperKey!);
    final tempDir = await getTemporaryDirectory();
    File file = await File('${tempDir.path}/$fileName').create();
    file.writeAsBytesSync(image!);
    profilePath  = file.path;
    uploadImageToServer(context);
  }

  uploadProfileDetail(File selectedImage) async {
    debugPrint("Image Path ===> ${selectedImage.path}");
    loading(currentPageContext);
    String fileName = selectedImage.path.split('/').last;
    FormData formData = FormData.fromMap({
      "profileBannerImageURL": await MultipartFile.fromFile(
          selectedImage.path,
          filename: fileName,
          contentType: MediaType("image", "png")),
    });

    try {
      var resp = await postImageApiCalling(
          formData, ApiEndPoints.postProfileBannerImage);
      handleResponseCode(resp);
    } on DioException catch (ex) {
      handleResponseCode(ex.response!);
    }
  }

  Future<String?> getImageDownload(profileUrl) async {
    var fileName = "image_${DateTime.now().millisecondsSinceEpoch}.png";
    dir = await getTemporaryDirectory();
    var resp = await downloadApiCalling('${dir?.path}/$fileName', profileUrl);
    if (resp.statusCode == 200) {
      currentApiState = APIState.success;
      return '${dir?.path}/$fileName';
    } else {
      currentApiState = APIState.failure;
    }
    return null;
  }

  void handleResponseCode(Response apiResp) {
    switch (apiResp.statusCode) {
      case 200:
        currentApiState = APIState.success;
        apiResponse = apiResp.data;
        onSuccess(currentPageContext, apiResponse);
        break;
      default:
        currentApiState = APIState.failure;
        apiResponse = apiResp.data['message'];
        onFailure(currentPageContext, apiResponse);
        break;
    }
  }

}
